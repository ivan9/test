## 5.1.2

- **FIX:** Cost Calculator Styles.
- Woocommerce plugin compatible with version (4.3.1).
- STM Importer updated (v 5.5.0).

## 5.1.1

- **NEW:** Add New Plugin "STM Templates Library" (Visual Composer Templates Library) 
- **FIX:** Not showing website property for Elementor's Portfolio Information widget.
- **FIX:** Iconbox setting 'Open in new tab' for Elementor.
- **FIX:** Missing custom bg color setting for Elementor's Iconbox widget.
- **FIX:** 'Works' widget breaking on adding categories.
- **FIX:** Removed from notice recommended builder plugin if another is installed.
- **FIX:** Add Settings for Black and White Images on [Melbourne](https://consulting.stylemixthemes.com/melbourne/) Layout.
- Updated Woocommerce plugin templates. Compatible with version 4.3.0.
- Updated Consulting ELementor Widgets plugin (v 1.0.2).


## 5.1.0

- Compatible with **WordPress 5.4.2**.
- **UPD:** [New York](https://consulting.stylemixthemes.com/) demo updated.
- **UPD:** [Los Angeles](https://consulting.stylemixthemes.com/los-angeles/) demo updated.
- **UPD:** [Melbourne](https://consulting.stylemixthemes.com/melbourne/) demo updated.
- **UPD:** [Marseille](https://consulting.stylemixthemes.com/marseille/) demo updated.
- **UPD:** [Lisbon](https://consulting.stylemixthemes.com/lisbon/) demo updated.
- **UPD:** [New Delhi](https://consulting.stylemixthemes.com/new-delhi/) demo updated.
- **UPD:** [Barcelona](https://consulting.stylemixthemes.com/barcelona/) demo updated.
- **UPD:** [Zurich](https://consulting.stylemixthemes.com/zurich/) demo updated.
- **UPD:** [London](https://consulting.stylemixthemes.com/two/) demo updated.
- **UPD:** [Mumbai](https://consulting.stylemixthemes.com/mumbai/) demo updated.
- **UPD:** [Liverpool](https://consulting.stylemixthemes.com/liverpool/) demo updated.
- **UPD:** [Budapest](https://consulting.stylemixthemes.com/budapest/) demo updated.
- **UPD:** [Berlin](https://consulting.stylemixthemes.com/berlin/) demo updated.
- **UPD:** [Shanghai](https://consulting.stylemixthemes.com/five/) demo updated.
- **FIX:** Elementor module Image (module picture changed to post featured image).
- **FIX:** Elementor module Text Editor (paragraph styles settings did not work).
- **FIX:** Languages fields ("categories:" word was not translated).
- Updated Woocommerce plugin templates. Compatible with version 4.2.0.
- STM Importer updated (v 5.4.0).


## 5.0.2

- Compatible with **WordPress 5.4**.
- **NEW:** [Geneva](http://consulting.stylemixthemes.com/geneva/) demo.
- WPBakery Page Builder updated (v 6.2.0).
- Revolution Slider updated (v 6.2.6).
- STM Custom Icons updated (v 3.0.2).
- STM Importer updated (v 5.3.2).


## 5.0.1

- **NEW:** [Liverpool](http://consulting.stylemixthemes.com/liverpool/) demo.
- **FIX:** Minor bug fixes.
- Booked updated (v 2.2.6).
- STM Importer updated (v 5.3.1).
- Consulting Elementor Widgets plugin added (v 1.0.1).


## 5.0

- **NEW:** Consulting WP works now both with **WP Baker** and **Elementor** Page Builders.
- **NEW:** **WPBakery** &gt; **Elementor** parser added.
- **FIX:** Minor bug fixes.
- Revolution Slider updated (v 6.1.8).
- STM Custom Icons updated (v 3.0).
- STM Post Type updated (v 3.0).
- STM Importer updated (v 5.3).
- Consulting Elementor Widgets plugin added (v 1.0).
- WooCommerce templates updated (v 4.0).


## 4.7

- **NEW:** [Frankfurt](http://consulting.stylemixthemes.com/three/) demo updated.
- **NEW:** [Hong Kong](http://consulting.stylemixthemes.com/ten/) demo updated.
- **NEW:** [Rome](http://consulting.stylemixthemes.com/twenty/) demo updated.
- **NEW:** [Ankara](http://consulting.stylemixthemes.com/ankara/) demo updated.
- **NEW:** [Barcelona](http://consulting.stylemixthemes.com/barcelona/) demo updated.
- **NEW:** [Berlin](http://consulting.stylemixthemes.com/berlin/) demo updated.
- **NEW:** [Davos](http://consulting.stylemixthemes.com/davos/) demo updated.
- **NEW:** [Denver](http://consulting.stylemixthemes.com/denver/) demo updated.
- **NEW:** [Istanbul](http://consulting.stylemixthemes.com/istanbul/) demo updated.
- **NEW:** [Lisbon](http://consulting.stylemixthemes.com/lisbon/) demo updated.
- **NEW:** [Lyon](http://consulting.stylemixthemes.com/lyon/) demo updated.
- **NEW:** [Marseille](http://consulting.stylemixthemes.com/marseille/) demo updated.
- **NEW:** [Melbourne](http://consulting.stylemixthemes.com/melbourne/) demo updated.
- **NEW:** [Milan](http://consulting.stylemixthemes.com/milan/) demo updated.
- **NEW:** [Osaka](http://consulting.stylemixthemes.com/osaka/) demo updated.
- **NEW:** [San Francisco](http://consulting.stylemixthemes.com/san-francisco/) demo updated.
- **NEW:** [Toronto](http://consulting.stylemixthemes.com/toronto/) demo updated.
- **NEW:** [Vienna](http://consulting.stylemixthemes.com/vienna/) demo updated.


## 4.6.9.3

- **NEW:** [Brussels](http://consulting.stylemixthemes.com/brussels/) demo updated.
- **NEW:** [Tokyo](http://consulting.stylemixthemes.com/seven/) demo updated.
- **NEW:** [Sydney](http://consulting.stylemixthemes.com/nine/) demo updated.
- **NEW:** [Moscow](http://consulting.stylemixthemes.com/four/) demo updated.
- **NEW:** [Sao Paulo](http://consulting.stylemixthemes.com/thirteen/) demo updated.
- **NEW:** [Abu Dhabi](http://consulting.stylemixthemes.com/fourteen/) demo updated.
- **NEW:** [Dublin](http://consulting.stylemixthemes.com/fifteen/) demo updated.
- **NEW:** [Rome](http://consulting.stylemixthemes.com/twenty/) demo updated.
- **NEW:** [Amsterdam](http://consulting.stylemixthemes.com/amsterdam/) demo updated.
- **NEW:** [Beijing](http://consulting.stylemixthemes.com/beijing/) demo updated.
- **NEW:** [New Delhi](http://consulting.stylemixthemes.com/new-delhi/) demo updated.
- **FIX:** Customizer Color change bug fixed.
- **FIX:** STM POst Type translation bug fixed.
- **FIX:** Minor bug fixes.
- STM Post Type updated (v 2.2.4).
- STM Importer updated (v 5.2.5).


## 4.6.9.2

- **NEW:** [Mumbai](http://consulting.stylemixthemes.com/mumbai/) demo updated.
- **NEW:** [London](http://consulting.stylemixthemes.com/two/) demo updated.
- **NEW:** [Madrid](http://consulting.stylemixthemes.com/six/) demo updated.
- **NEW:** [Seoul](http://consulting.stylemixthemes.com/eight/) demo updated.
- **NEW:** [Paris](http://consulting.stylemixthemes.com/eleven/) demo updated.
- **NEW:** [Singapore](http://consulting.stylemixthemes.com/twelve/) demo updated.


## 4.6.9.1

- **NEW:** [New York](http://consulting.stylemixthemes.com/) demo updated.
- **NEW:** [Los Angeles](http://consulting.stylemixthemes.com/los-angeles/) demo updated.
- **NEW:** [Shanghai](http://consulting.stylemixthemes.com/five/) demo updated.
- **NEW:** [Stockholm](http://consulting.stylemixthemes.com/stockholm/) demo updated.
- **NEW:** [Zurich](http://consulting.stylemixthemes.com/zurich/) demo updated.


## 4.6.9

- **NEW:** [Budapest](http://consulting.stylemixthemes.com/budapest/) demo.
- Visual Composer updated (v 6.1).
- Revolution Slider updated (v 6.1.5).
- STM Custom Icons updated (v 2.9.3).
- STM Post Type updated (v 2.2.3).
- STM Importer updated (v 5.2.4).


## 4.6.8.1

- Compatible with **WordPress 5.3**.
- Revolution Slider updated (v 6.1.4).
- WooCommerce templates updated (v 3.8).


## 4.6.8

- **NEW:** [Ankara](http://consulting.stylemixthemes.com/ankara/) demo.
- **NEW:** [Osaka](http://consulting.stylemixthemes.com/osaka/) demo.
- Revolution Slider updated (v 6.1.3).
- STM Importer updated (v 5.2.3).


## 4.6.7

- **NEW:** [Barcelona](http://consulting.stylemixthemes.com/barcelona/) demo.
- Revolution Slider updated (v 6.1.2).
- STM Importer updated (v 5.2.1).


## 4.6.6

- **NEW:** [Melbourne](http://consulting.stylemixthemes.com/melbourne/) demo.
- **NEW:** [Lyon](http://consulting.stylemixthemes.com/lyon/) demo.
- Revolution Slider updated (v 6.1.0).
- STM Importer updated (v 5.2).
- Language files updated.


## 4.6.5

- **NEW:** [Stockholm](http://consulting.stylemixthemes.com/stockholm/) demo.
- Revolution Slider updated (v 6.0.9).
- STM Custom Icons updated (v 2.9.2).
- STM Importer updated (v 5.1.9).
- STM Post Type updated (v 2.2.2).


## 4.6.4

- **NEW:** [Lisbon](http://consulting.stylemixthemes.com/lisbon/) demo.
- Visual Composer updated (v 6.0.5).
- Revolution Slider updated (v 6.0.7).
- STM Custom Icons updated (v 2.9.1).
- STM Importer updated (v 5.1.8).


## 4.6.3

- **NEW:** [Berlin](http://consulting.stylemixthemes.com/berlin/) demo.
- STM Importer updated (v 5.1.7).


## 4.6.2

- Visual Composer updated (v 6.0.3).
- Booked plugin updated (v 2.2.5).
- STM Post Type updated (v 2.2.1).


## 4.6.1

- Compatible with **WordPress 5.2**.
- Visual Composer updated (v 6.0.2).
- STM Post Type updated (v 2.2).
- **FIX:** Minor bug fixes.


## 4.6

- **NEW:** [Marseille](http://consulting.stylemixthemes.com/marseille/) demo.
- STM Importer updated (v 5.1.6).
- Revolution Slider updated (v 4.5.8.3).
- Booked plugin updated (v 2.2.4).
- WooCommerce templates updated (v 3.6).


## 4.5.6

- Booked plugin updated (v 2.2.3).
- **FIX:** Header phone number issue fixed.
- **FIX:** Icon Box displaying headings bug fixed.
- **FIX:** Minor bug fixes.


## 4.5.5

- Visual Composer updated (v 5.7).
- Revolution Slider updated (v 4.5.8.2).
- STM Importer updated (v 5.1.5).
- STM Post Type updated (v 2.1).


## 4.5.4

- **NEW:** [Vienna](http://consulting.stylemixthemes.com/vienna/) demo.
- STM Importer updated (v 5.1.4).


## 4.5.3

- **NEW:** [Milan](http://consulting.stylemixthemes.com/milan/) demo.
- STM Importer updated (v 5.1.3).


## 4.5.2

- **NEW:** [New Delhi](http://consulting.stylemixthemes.com/new-delhi/) demo.
- STM Importer updated (v 5.1.2).


## 4.5.1

- Compatible with **WordPress 5.0**.
- **NEW:** [Istanbul](http://consulting.stylemixthemes.com/istanbul) demo.
- Revolution Slider updated (v 5.4.8.1).
- Visual Composer updated (v 5.6).
- Booked plugin updated (v 2.2.2).
- STM Importer updated (v 5.1.1).


## 4.5

- **NEW:** [Beijing](http://consulting.stylemixthemes.com/beijing) demo.
- STM Importer updated (v 5.1).


## 4.4

- **NEW:** [San Francisco](http://consulting.stylemixthemes.com/san-francisco) demo.
- STM Importer updated (v 5.0).


## 4.3

- **NEW:** [Toronto](http://consulting.stylemixthemes.com/toronto) demo.
- STM Importer updated (v 4.9).
- STM Custom Icons updated (v 2.6).


## 4.2

- **NEW:** [Los Angeles](http://consulting.stylemixthemes.com/los-angeles) demo.
- STM Importer updated (v 4.8).
- WooCommerce templates updated (v 3.5.1).


## 4.1.7

- Visual Composer updated (v 5.5.5).
- WooCommerce templates updated (v 3.5.0).
- **NEW:** [Custom Post Type](https://support.stylemixthemes.com/manuals/consulting/#customization) options improved.
- **NEW:** [Custom Footer](https://support.stylemixthemes.com/manuals/consulting/#footer_area) options included.
- **FIX:** Popup Maker plugin and Consulting Mega Menu conflicts fixed.
- **FIX:** Minor bug fixes.


## 4.1.6

- **NEW DEMO:** [Brussels](http://consulting.stylemixthemes.com/brussels/).
- STM Importer plugin updated (v 4.7).


## 4.1.5

- STM Importer plugin updated (v 4.6).
- Revolution Slider updated (v 5.4.8).
- Visual Composer updated (v 5.5.2).
- WooCommerce templates updated (v 3.4.3).
- **FIX:** Mobile menu 3-rd level issue fixed.
- **FIX:** Pricing Plan element issues fixed.
- **FIX:** Minor bug fixes.


## 4.1.3

- **NEW DEMO:** [Denver](http://consulting.stylemixthemes.com/denver/).
- **NEW:** [WP GDPR Compliance](https://wordpress.org/plugins/wp-gdpr-compliance) plugin included.
- Custom Icons plugin updated (v 2.3).
- STM Importer plugin updated (v 4.5).
- Revolution Slider updated (v 5.4.7.3).


## 4.1.2

- **NEW DEMO:** [Davos](http://consulting.stylemixthemes.com/davos).
- Custom Icons plugin updated (v 2.2).
- STM Importer plugin updated (v 4.4).


## 4.1.1

- **FIX:** Conflict Header Builder plugin fixed.
- **FIX:** Consulting Dashboard &gt; big "C" logo problem fixed.


## 4.1

- **NEW DEMO:** [Amsterdam](http://consulting.stylemixthemes.com/amsterdam).
- **NEW:** [Demo Installation](https://stylemixthemes.com/manuals/consulting/#demo_import) is Faster and Easier now!
- Booked plugin updated (v 2.1).
- Custom Icons plugin updated (v 2.1).
- STM Importer plugin updated (v 4.3).
- Revolution Slider updated (v 5.4.7.1).
- **FIX:** Minor bug fixes.


## 4.0.5

- **NEW DEMO:** [Mumbai](http://consulting.stylemixthemes.com/mumbai).
- Booked plugin updated (v 2.0.10).
- Custom Icons plugin updated (v 2.0).
- STM Importer plugin updated (v 4.2).
- Visual Composer updated (v 5.4.7).


## 4.0.4

- **NEW DEMO:** [Zurich](http://consulting.stylemixthemes.com/zurich).
- STM Importer plugin updated (v 4.1).


## 4.0.3

- Compatible with WordPress 4.9.4.
- Booked plugin updated (v 2.0.9).
- WooCommerce templates updated (v 3.3).
- **FIX:** Minor bug fixes.


## 4.0.2

- Child Theme updated (4.0.2).
- Revolution Slider updated (v 5.4.6.4).
- Booked plugin updated (v 2.0.7).
- **FEATURE:** Join Event email notification improved.
- **FIX:** Rome demo custom colors problem fixed.
- **FIX:** Blog post Author BIO displaying issue fixed.


## 4.0.1

- WooCommerce templates updated.
- **NEW:** Ready translation files for **Dutch** language included.
- **FIX:** Minor bug fixes.


## 4.0

- Compatible with WordPress 4.9.
- Visual Composer updated (v 5.4.5).
- STM Importer plugin updated (v 4.0).
- STM Post Type plugin updated (v 2.0).
- **NEW:** Ready translation files for **Turkish** language included.


## 3.9.3

- WooCommerce templates updated (v 3.2.3).
- STM Importer updated (v 2.5).


## 3.9.2

- **AMP supported**.
- Visual Composer updated (v 5.4.2).
- Revolution Slider updated (v 5.4.6.2).
- Booked plugin updated (v 2.0.6).
- **BONUS:** Editable corporate identity templates included.


## 3.9.1

- WooCommerce templates updated (v 3.2).
- Child theme updated.
- **FIX:** Minor bug fixes.


## 3.9

- STM Post Type plugin updated (v 1.6).
- Demo content updated.


## 3.8

- Visual Composer updated (v 5.3).
- Revolution Slider updated (v 5.4.6).


## 3.7.9

- WooCommerce templates updated.
- **FIX:** Modern Event load more bug fixed.
- **FIX:** WooCommerce Ajax Cart bug fixed.


## 3.7.8

- Booked plugin updated (v 2.0.5).
- **NEW:** Added Category filter for STM Services module.
- **FIX:** Minor bug fixes.


## 3.7.7

- Compatible with WordPress 4.8.1.
- Visual Composer updated (v 5.2.1).
- Demo content updated.


## 3.7.6

- **FIX:** Bug between PHP 5.3 and Mega Menu fixed.
- **FIX:** Bug with Child Theme fixed.


## 3.7.5

- Booked plugin updated (v 2.0.3).
- Demo sliders updated.
- **NEW:** **Custom Mega Menu** feature included.
- **FIX:** Event forms bug fixed.


## 3.7.4

- Booked plugin updated (v 1.9.15).
- Revolution Slider updated (v 5.4.5.1).
- STM Importer updated (v 2.4).
- STM Post Type plugin updated (v 1.5).
- **FIX:** WooCommerce Cart widget bug fixed.


## 3.7.3

- Compatible with WordPress 4.8.
- **FIX:** Cases with Filter appearance bug fixed.


## 3.7.2

- **NEW DEMO:** **Rome**
- WooCommerce templates updated.


## 3.7.1

- **NEW:** Import widgets with demo content.
- **FIX:** Minor bug fixes.
- Revolution Slider updated (v 5.4.3.1).


## 3.7

- **NEW DEMO:** **Oslo**
- Revolution Slider updated (v 5.4.3).
- Booked plugin updated (v 1.9.12).


## 3.6

- Compatible with WooCommerce 3.0.
- Visual Composer updated (v 5.1.1).
- **NEW:** Event Lessons module added.
- **FIX:** VC Frontend Editor bug fixed.


## 3.5.7

- Visual Composer updated (v 5.1).
- Revolution Slider updated (v 5.4.1).
- Custom Icons by Stylemixthemes plugin updated (v 1.6).
- STM Post Type plugin updated (v 1.4).
- **FIX:** Boxed version responsiveness bug fixed.
- **FIX:** Contact Form 7 - Recaptcha problem fixed.
- **FIX:** Custom Colors bug fixed.


## 3.5.6

- **FEATURE:** Page Options - Title Box background color.
- **FEATURE:** Custom Link option is included for Info Box.
- **FEATURE:** Filter Events by event date: Past Events, Upcoming Events and All Events.
- STM Post Type plugin is updated (v 1.3).
- **FIX:** Minor bug fixes.


## 3.5.5

- **NEW:** Post types: **Portfolio** and **Events**.
- **NEW:** Pages: About us, Contacts, Events, Portfolio and Make an Appointment.
- Font Awesome icons updated.
- STM Importer plugin updated (v 2.1).
- STM Post Type plugin updated (v 1.2).
- Custom Icons by Stylemixthemes updated (v 1.6).
- Revolution Slider updated (v 5.3.1.5).


## 3.5.4

- **NEW:** Theme Activation feature via Dashboard.
- **FEATURE:** WPML language switcher custom styles.
- **FIX:** Minor bug fixes.


## 3.5.3

- Revolution Slider is updated (v 5.3.1).
- Compatible with WordPress 4.7.
- **FIX:** Sticky menu script is corrected for smooth scrolling.
- **FIX:** Bug with Custom Icons Manager is fixed.


## 3.5.2

- Visual Composer plugin updated (v 5.0.1).
- Revolution Slider updated (v 5.3.0.2).
- STM Importer updated (v 1.9).
- **NEW:** Staff List module – Add Filter by Category(es).


## 3.5.1

- Visual Composer (4.12.1), STM Importer (1.8) and Custom Icons by Stylemixthemes (1.5) plugins updated.
- **NEW:** Category taxonomy is included for Staff post type.
- **NEW:** Custom Links included for Info Box and Images Carousel modules.
- **NEW:** Autoplay option is included for Testimonials carousel module.
- **FIX:** Minor bug fixes.


## 3.5

- **New RTL Layouts:** 3 Extra Layouts: **Tel Aviv, Dubai, Tehran**.
- **FIX:** Small bug fixes.


## 3.4

- **NEW DEMO:** 3 Extra Layouts: **Sao Paulo, Abu Dhabi, Dublin**.
- STM Post Type and STM Importer plugins updated.
- WooCommerce templates updated up to 2.6.4 version.


## 3.3.2

- Google Map API key option is included.
- **FIX:** VC Gallery bug with slider arrows is fixed.


## 3.3.1

- WooCommerce tabs on My Account page are included.
- **FIX:** Bug with Update Cart button is fixed.


## 3.3

- **NEW:** 7 Language files added: **German, French, Italian, Portuguese, Spanish, Russian, Persian/Arabic**.
- **NEW DEMO:** 3 Extra Layouts: **Hong Kong, Paris, Singapore**.
- Visual Composer, Revolution slider and STM plugins updated.
- WooCommerce templates updated.
- Demo content updated.


## 2.2

- **NEW:** RTL Support – Added.
- **NEW:** WooCommerce Cart Icon switchers have been added in Customizer.
- **NEW:** Default Page Options settings in Customizer.
- **FEATURE:** Compatible with Gravity Form plugin.
- **FEATURE:** Compatible with bbPress forum with full design integration.
- **FIX:** Bugs with Top Bar Contact Info fields, MetaBox image and Media Properties are fixed.
- Layout Three demo content is updated.


## 2.1

- **FEATURE:** Changed Info Box Style &amp; Demo Content for Demo Three.
- **FIX:** Fixed customizer conflict with ui tabs.
- **FIX:** Twitter Widget Styles bug is fixed.
- **FIX:** Bug with PHP 5.3 fixed.


## 2.0

- **NEW DEMO:** Nine custom layouts are added.
- Visual Composer plugin updated.
- STM Importer plugin updated.
- Revolution Slider plugin updated.


## 1.2.1

- Visual Composer updated.
- **FIX:** STM Charts module bugs are fixed.


## 1.2

- Compatible with WordPress 4.5.
- Visual Composer updated.
- Revolution Slider updated.
- **FIX:** Small bug fixes.


## 1.1

- STM Importer updated.
- Visual Composer updated.
- Revolution Slider updated.
- STM Map Zoom bug is fixed.
- Child theme updated.


